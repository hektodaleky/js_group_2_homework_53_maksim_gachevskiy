import React, {Component} from "react";
import "./App.css";
import AddNewTask from "./Task/AddNewTask";
import Task from "./Task/Task";

class App extends Component {
    state = {
        tasks: [
            {text: 'My first default task', id: '6cascsa', val: false},
            {text: 'My second default task', id: '7cascs', val: false},
            {text: 'My third default task', id: '678cascascax', val: false},
            {text: 'My fourth default task', id: '678cas6', val: false},
        ],
    };

    addTask = () => {
        let inputValue = document.getElementById('addTextInput').value;
        if (!inputValue)
            return;
        const newEvent = {
            text: inputValue,
            id: Date.now(),
            val: false
        };
        document.getElementById('addTextInput').value = "";

        const events = [...this.state.tasks];
        events.push(newEvent);

        this.setState({tasks: events});
    };
    removeTask = (id) => {

        const index = this.state.tasks.findIndex(p => p.id === id);
        const tasks = [...this.state.tasks];
        if (!tasks[index].val)
            alert("Вы удаляете ещё не выполненое задание");
        tasks.splice(index, 1);

        this.setState({tasks});
    };

    changeState = (event, id) => {
        const index = this.state.tasks.findIndex(p => p.id === id);
        const task = {...this.state.tasks[index]};
        task.val = event.target.checked;

        const tasks = [...this.state.tasks];
        tasks[index] = task;

        this.setState({tasks});

    };

    render() {
        let task = null;
        task = (<div>{

            this.state.tasks.map((oneTask) => {
                return (




                    <Task text={oneTask.text}
                          key={oneTask.id}
                          remove={(event) => {
                              event.preventDefault();
                              this.removeTask(oneTask.id)
                          }}
                          change={(event) => {
                              this.changeState(event, oneTask.id)
                          }}/>
                );

            })
        }</div>);
        return (
            <div className="App">
                <AddNewTask add={this.addTask}/>
                {task}
            </div>
        );


    }
}
;
export default App;
