/**
 * Created by Max on 17.01.2018.
 */
import React from 'react';
const AddNewTask=(props)=>{
    return(
        <div className="AddNewTask">
            <input placeholder="Введите задачу" id="addTextInput" type="text"/>
            <button onClick={props.add}>Add</button>
        </div>
    );
};

export default AddNewTask;