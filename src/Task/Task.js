/**
 * Created by Max on 17.01.2018.
 */
import React from "react";
import "./Task.css";
const Task = (props) => {
    return (
        <div className="Task">
            <p>{props.text}</p>


            <a href="" onClick={props.remove}>✘</a>
            <input type="checkbox" onClick={props.change}></input>


        </div>
    );
};
export default Task;